package indiceIncumplimiento;

import clase.ClasesJavaBean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.ejb.*;
import sis.conexion.Conexion;

//IndiceCumplimiento1RemoteBusiness
public class IndiceCumplimiento1Bean implements SessionBean {
    private SessionContext context;
    Connection conEns = null;
    ResultSet rs = null;
    Statement st = null;   
    private Conexion myConexion = new Conexion();
    

    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }   
              
    public int atiempo(String fechaIni, String fechaFin ,String comparacion,String dia,String dia1, String rut, String fecha){
         int atiempo = 0; 
         String sql="";
        try{
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            if(dia1.equals("")){            
            sql = sql + " SELECT count(Datediff(dd,FechaCli, "+ fecha +")) as sin_atraso \n";
            sql = sql + " from TPMP a, TComercial b                                      \n";            
            sql = sql + " where Datediff(dd, a.FechaCli, "+ fecha +") " + comparacion +" '" + dia +"' \n";
            sql = sql + " and b.RutCliente "+ rut +" '96543670' and a.Pedido=b.Pedido            \n";           
            sql = sql + " and a.Linea=b.Linea  and a.Familia not in( 'OTRO','CELDA')      \n";                                                  
            sql = sql + " and "+ fecha +" >=   '"+fechaIni+"'                             \n";                                            
            sql = sql + " and "+ fecha +" <=   '"+fechaFin+"'                             \n"; 
            sql = sql + " and a.Codpro NOT LIKE 'B%'                                      \n";
            }else{           
            sql = sql + " SELECT count(Datediff(dd,FechaCli, "+ fecha +")) as sin_atraso  \n";
            sql = sql + " from TPMP a, TComercial b                                       \n";            
            sql = sql + " where Datediff(dd, a.FechaCli, "+ fecha +") " + comparacion +" '" + dia +"' and '" +dia1 +"' \n";
            sql = sql + " and b.RutCliente "+ rut +" '96543670' and a.Pedido=b.Pedido              \n";           
            sql = sql + " and a.Linea=b.Linea  and a.Familia not in( 'OTRO','CELDA')      \n";                                                  
            sql = sql + " and "+ fecha +" >=   '"+fechaIni+"'                             \n";                                            
            sql = sql + " and "+ fecha +" <=   '"+fechaFin+"'                             \n"; 
            sql = sql + " and a.Codpro NOT LIKE 'B%'                                      \n";
            }
            rs = st.executeQuery(sql);
            while(rs.next()){
                atiempo = rs.getInt(1);
                }
            rs.close(); 
            conEns.close();
            st.close();
        }catch(Exception e){   
               e.printStackTrace();                        
        }                                          
        return atiempo;
    }
        
    /*ingreso() entrega la cuenta total de equipos
     *que han ingresado en bodega
     **/
    public int ingreso(String fechaIni, String fechaFin, String comparacion, String fecha){
         int ingreso = 0;
        try{
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            String sql = "";
            sql = sql + " select count(1)as en_bodega_CAM                          \n";
            sql = sql + " from TPMP a,TComercial b                                 \n";
            sql = sql + " where a.Pedido=b.Pedido and a.Linea=b.Linea              \n";
            sql = sql + " and a.Familia <> 'OTRO' and a.Familia <> 'CELDA'         \n"; 
            sql = sql + " and b.RutCliente " + comparacion + " '96543670'          \n";
            sql = sql + " and a.FechaCli is not null and " + fecha + " is not null \n";
            sql = sql + " and " + fecha + "  >= '"+fechaIni+"'                     \n"; 
            sql = sql + " and " + fecha + " <= '"+fechaFin+"'                      \n";
            sql = sql + " and a.Codpro NOT LIKE '%B%'                              \n";          
            rs = st.executeQuery(sql);          
            while(rs.next()){
                ingreso = rs.getInt(1);
                }         
            rs.close(); 
            conEns.close();
            st.close();      
        }catch(Exception e){                   
            e.printStackTrace();                        
        }                                          
        return ingreso;
    }
         
        public Vector cargarEquiposBodega(String fechaIni, String fechaFin, String comparacion, String fecha, String cliente ){
        Vector v1 = new Vector();
        try{
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            String sql = "";
            sql = sql + " select a.Pedido,a.Linea,a.Serie,a.Descri,a.Expeditacion,    \n";   
            sql = sql + " a.FechaCli, " + fecha + ", b.CartaAviso,                    \n";   
            sql = sql + " b.OrdenDeCompra,a.Familia,a.KVA,a.FechaBodega,b.PorcMulta,  \n";
            sql = sql + " a.Multa, b.DiasAtraso, b.ValorMulta, a.Proyecto, a.Monto,   \n";
            sql = sql + " Datediff(dd, a.FechaCli, " + fecha + ")  , a.Cliente        \n";
            sql = sql + " from TPMP a,TComercial b                                    \n";
            sql = sql + " where a.Pedido=b.Pedido and a.Linea=b.Linea                 \n";
            sql = sql + " and a.Familia not in ('CELDA','OTRO')                       \n"; 
            sql = sql + " and b.RutCliente " + comparacion + " '96543670'             \n";
            sql = sql + " and a.FechaCli is not null                                  \n";
            sql = sql + " and " + fecha + " is not null                               \n";
            sql = sql + " and " + fecha + " >= '"+fechaIni+"'                         \n";
            sql = sql + " and " + fecha + " <= '"+fechaFin+"'                         \n";
            sql = sql + " and a.Codpro NOT LIKE 'B%'                                  \n";
            sql = sql + " order by a.Pedido  asc                                      \n";
            rs = st.executeQuery(sql);
            //System.out.println(sql);
            while(rs.next()){
                clase.ClasesJavaBean des1 = new clase.ClasesJavaBean();
                des1.setPedido(rs.getInt(1));
                des1.setLinea(rs.getInt(2));
                des1.setSerie(rs.getString(3));
                des1.setDescri(rs.getString(4));
                des1.setExpeditacion(rs.getDate(5));
                des1.setFechaCli(rs.getDate(6));
                des1.setFechaAviso(rs.getDate(7));
                des1.setCartaAviso(rs.getString(8));
                des1.setOrdenDeCompra(rs.getString(9));
                des1.setFamilia(rs.getString(10));
                des1.setKVA(rs.getString(11));
                des1.setFechaBodega(rs.getDate(12));
                des1.setPorcMulta(rs.getFloat(13));
                des1.setMulta(rs.getInt(14));
                des1.setDiasAtraso(rs.getFloat(15));
                des1.setValorMulta(rs.getFloat(16));
                des1.setProyecto(rs.getString(17));
                des1.setMonto(rs.getFloat(18));
                des1.setAtraso(rs.getInt(19));
                if(cliente.equals("C")){
                des1.setCliente(rs.getString(20));    
                }
                v1.addElement(des1);
            }
            rs.close();
            st.close();
        }catch(Exception e){
               // e.printStackTrace();
                System.out.println("Error :  cargarEquiposBodega() " + e.getMessage() );
        }
        return v1;
    }
        
        public Vector mostrarDetalleEquipo(String fechaIni, String fechaFin, String comparacion, String fecha, String cliente, String signo, String dia, String dia1){
        Vector v1 = new Vector();
        try{
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            String sql = "";
            sql = sql + " select a.Pedido,a.Linea,a.Serie,a.Descri,a.Expeditacion,     \n";   
            sql = sql + " a.FechaCli, " + fecha + ", b.CartaAviso,                     \n";   
            sql = sql + " b.OrdenDeCompra,a.Familia,a.KVA,a.FechaBodega,b.PorcMulta,   \n";
            sql = sql + " a.Multa, b.DiasAtraso, b.ValorMulta, a.Proyecto, a.Monto,    \n";
            sql = sql + " Datediff(dd, a.FechaCli, " + fecha + "), a.Cliente           \n";
            sql = sql + " from TPMP a,TComercial b                           \n";
            if(dia1.equals("")){
            sql = sql + " where Datediff(dd, a.FechaCli, " + fecha + ") " + signo + " " + dia + " \n";
            }else{
             sql = sql + " where Datediff(dd, a.FechaCli, " + fecha + ") " + signo + " " + dia + " and " +  dia1 + " \n";
            }
            sql = sql + " and a.Pedido=b.Pedido and a.Linea=b.Linea          \n";
            sql = sql + " and a.Familia not in ('CELDA','OTRO')              \n"; 
            sql = sql + " and b.RutCliente "+ comparacion +" '96543670'      \n";
            sql = sql + " and a.FechaCli is not null                         \n";
            sql = sql + " and " + fecha + " is not null                      \n";
            sql = sql + " and " + fecha + " >= '"+fechaIni+"'                \n";
            sql = sql + " and " + fecha + " <= '"+fechaFin+"'                \n";
            sql = sql + " and a.Codpro NOT LIKE 'B%'                         \n";
            sql = sql + " order by a.Pedido  asc                             \n";
            
           
            rs = st.executeQuery(sql);
            while(rs.next()){
                clase.ClasesJavaBean des1 = new clase.ClasesJavaBean();
                des1.setPedido(rs.getInt(1));
                des1.setLinea(rs.getInt(2));
                des1.setSerie(rs.getString(3));
                des1.setDescri(rs.getString(4));
                des1.setExpeditacion(rs.getDate(5));
                des1.setFechaCli(rs.getDate(6));
                des1.setFechaAviso(rs.getDate(7));
                des1.setCartaAviso(rs.getString(8));
                des1.setOrdenDeCompra(rs.getString(9));
                des1.setFamilia(rs.getString(10));
                des1.setKVA(rs.getString(11));
                des1.setFechaBodega(rs.getDate(12));
                des1.setPorcMulta(rs.getFloat(13));
                des1.setMulta(rs.getInt(14));
                des1.setDiasAtraso(rs.getFloat(15));
                des1.setValorMulta(rs.getFloat(16));
                des1.setProyecto(rs.getString(17));
                des1.setMonto(rs.getFloat(18));
                des1.setAtraso(rs.getInt(19));
                if(cliente.equals("C")){
                des1.setCliente(rs.getString(20));
                }
                v1.addElement(des1);
            }
            rs.close();
            st.close();
            conEns.close();
        }catch(Exception e){
                e.printStackTrace();
        }
        return v1;
    }
    
    public void ejbActivate() {
        
    }
   
    public void ejbPassivate() {
        
    }    
  
    public void ejbRemove() {
        
    }
  
    public void ejbCreate() {
    }
    
    public int multiplica (){
        int multi = 100;
        return multi;
    }       
}
