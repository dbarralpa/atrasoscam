
package indiceIncumplimiento;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for IndiceCumplimiento1 enterprise bean.
 */
public interface IndiceCumplimiento1RemoteHome extends EJBHome {
    
    IndiceCumplimiento1Remote create()  throws CreateException, RemoteException;
    
    
}
