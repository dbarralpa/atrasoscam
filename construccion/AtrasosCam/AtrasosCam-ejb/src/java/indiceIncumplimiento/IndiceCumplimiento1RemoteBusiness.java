
package indiceIncumplimiento;


/**
 * This is the business interface for IndiceCumplimiento1 enterprise bean.
 */
public interface IndiceCumplimiento1RemoteBusiness {
    
    int atiempo(String fechaIni, String fechaFin, String comparacion, String dia, String dia1, String rut, String fecha) throws java.rmi.RemoteException;
    int ingreso(String fechaIni, String fechaFin, String comparacion, String fecha) throws java.rmi.RemoteException;
    java.util.Vector cargarEquiposBodega(String fechaIni, String fechaFin, String comparacion, String fecha, String cliente) throws java.rmi.RemoteException;
    java.util.Vector mostrarDetalleEquipo(String fechaIni, String fechaFin, String comparacion, String fecha, String cliente, String signo, String dia, String dia1) throws java.rmi.RemoteException;
     int multiplica() throws java.rmi.RemoteException;
}
