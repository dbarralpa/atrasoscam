
package indiceIncumplimiento;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for IndiceCumplimiento1 enterprise bean.
 */
public interface IndiceCumplimiento1Remote extends EJBObject, IndiceCumplimiento1RemoteBusiness {
        
}
