/*
 * Contador.java
 *
 * Created on 1 de febrero de 2011, 08:34 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.contador;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 *
 * @author dbarra
 */
public class Contador {
    public Contador() {
        
    }
     public static void insertarContador(int recurso, String user){

          try{
            Statement st = null;
            ResultSet rs = null;
            Connection con=null;
            Conexion mConexion = new Conexion();
            con = mConexion.getENSConnector();
            st = con.createStatement(); 
            String sql="insert into MAC2BETA.dbo.TBL_SIS_TRAFICO(NB_ID_RECURSO, DT_FECHA, VC_USER) values ('" + recurso + "',getdate(),'" + user + "') ";
            st.execute(sql);   
            st.close();
            mConexion.cerrarConENS();
        }catch(Exception e){
             new Loger().logger("AtrasosCam.class " , " insertarContador() "+e.getMessage(),0);
        }
    }
}
