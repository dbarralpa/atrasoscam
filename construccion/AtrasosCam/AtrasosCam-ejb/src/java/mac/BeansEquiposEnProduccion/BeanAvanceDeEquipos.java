/*
 * BeanAvanceDeEquipos.java
 *
 * Created on 31 de agosto de 2007, 02:54 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.BeansEquiposEnProduccion;

/**
 *
 * @author pgonzalezc
 */
public class BeanAvanceDeEquipos {
    
    /** Creates a new instance of BeanAvanceDeEquipos */
    public BeanAvanceDeEquipos() {
    }
    
    private String Semielaborado;
    private String FechaInicio;
    private String FechaTermino;
    private String Observacion;

    public String getSemielaborado() {
        return Semielaborado;
    }

    public void setSemielaborado(String Semielaborado) {
        this.Semielaborado = Semielaborado;
    }

    public String getFechaInicio() {
        return FechaInicio;
    }

    public void setFechaInicio(String FechaInicio) {
        this.FechaInicio = FechaInicio;
    }

    public String getFechaTermino() {
        return FechaTermino;
    }

    public void setFechaTermino(String FechaTermino) {
        this.FechaTermino = FechaTermino;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }
    
}
