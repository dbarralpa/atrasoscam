/*
 * BeanExpeditacionDeEquipos.java
 *
 * Created on 31 de agosto de 2007, 12:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.BeansEquiposEnProduccion;

/**
 *
 * @author pgonzalezc
 */
public class BeanExpeditacionDeEquipos {
    
    /** Creates a new instance of BeanExpeditacionDeEquipos */
    public BeanExpeditacionDeEquipos() {
    }
    private String Fecha;
    private String Usuario;
    private String FechaReprogramacion;
    private String Observacion;

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getFechaReprogramacion() {
        return FechaReprogramacion;
    }

    public void setFechaReprogramacion(String FechaReprogramacion) {
        this.FechaReprogramacion = FechaReprogramacion;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }
    
}
