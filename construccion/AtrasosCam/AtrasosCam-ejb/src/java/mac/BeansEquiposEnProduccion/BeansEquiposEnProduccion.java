/*
 * BeansEquiposEnProduccion.java
 *
 * Created on 31 de agosto de 2007, 09:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.BeansEquiposEnProduccion;

/**
 *
 * @author pgonzalezc
 */
public class BeansEquiposEnProduccion {
    
    /** Creates a new instance of BeansEquiposEnProduccion */
    public BeansEquiposEnProduccion() {
    }
    private float Pedido;
    private float Linea;
    private String Fecha;
    private String FechaCli;
    private String Expeditacion;
    private String FechaPMP;
    private String Status;
    private String Condicion;
    private String Cliente;

    public float getPedido() {
        return Pedido;
    }

    public void setPedido(float Pedido) {
        this.Pedido = Pedido;
    }

    public float getLinea() {
        return Linea;
    }

    public void setLinea(float Linea) {
        this.Linea = Linea;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getFechaCli() {
        return FechaCli;
    }

    public void setFechaCli(String FechaCli) {
        this.FechaCli = FechaCli;
    }

    public String getExpeditacion() {
        return Expeditacion;
    }

    public void setExpeditacion(String Expeditacion) {
        this.Expeditacion = Expeditacion;
    }

    public String getFechaPMP() {
        return FechaPMP;
    }

    public void setFechaPMP(String FechaPMP) {
        this.FechaPMP = FechaPMP;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getCondicion() {
        return Condicion;
    }

    public void setCondicion(String Condicion) {
        this.Condicion = Condicion;
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }
    
}
