
package mac.equipoenproduccion;


/**
 * This is the business interface for EquipoEnProduccion enterprise bean.
 */
public interface EquipoEnProduccion1RemoteBusiness {
    java.util.Vector listarPedidosEquiposEnProduccion(String Pedido) throws java.rmi.RemoteException;
    java.util.Vector listarExpeditacionEquiposEnProduccion(String Pedido, String Linea) throws java.rmi.RemoteException;
    java.util.Vector listarAvancesEquiposEnProduccion(String Pedido, String Linea) throws java.rmi.RemoteException;
    
}
