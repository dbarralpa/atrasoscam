package mac.equipoenproduccion;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for EquipoEnProduccion enterprise bean.
 */
public interface EquipoEnProduccion1RemoteHome extends EJBHome {
    
    EquipoEnProduccion1Remote create()  throws CreateException, RemoteException;
    
    
}

