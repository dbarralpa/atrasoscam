package mac.equipoenproduccion;

import javax.ejb.*;
import java.beans.Beans;
import mac.BeansEquiposEnProduccion.*;
import mac.equipoenproduccion.*;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import sis.conexion.Conexion;

/**
 * This is the bean class for the EquipoEnProduccionBean enterprise bean.
 * Created 04-09-2007 08:49:53 AM
 * @author pgonzalezc
 */
public class EquipoEnProduccion1Bean implements SessionBean{//, EquipoEnProduccionRemoteBusiness {
    private SessionContext context;
    Connection conEns = null;
    ResultSet rs = null;
    Statement st = null;   
    private Conexion myConexion = new Conexion();
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
     
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    
      
        // Listar todos los Pedidos con su respectiva linea
        public Vector listarPedidosEquiposEnProduccion(String Pedido){
        int i=0;
        Vector vec= new Vector();
        try{
            //conectarShaffner();
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            //System.out.println("HOLAAAA");
            //System.out.println("ENTRO A listarPedidosEquiposEnProduccion()");
            String sql="";
            sql = sql + " select  Pedido,Linea,convert(varchar,Fecha,111),  \n";
            sql = sql + "         convert(varchar,FechaCli,111),            \n";
            sql = sql + "         convert(varchar,Expeditacion,111),        \n";
            sql = sql + "         convert(varchar,FechaPMP,111),            \n";
            sql = sql + "         Status,Condicion,Cliente                  \n";
            sql = sql + " from    ENSchaffner.dbo.TPMP                      \n";
            sql = sql + " where   Activa<>1                                 \n";
            sql = sql + " and     Estado<>1                                 \n";
            sql = sql + " and     Pedido="+Pedido+"                         \n";
            sql = sql + " order   by Pedido,Linea                           \n";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while(rs.next()){
                BeansEquiposEnProduccion bep = new BeansEquiposEnProduccion();
                bep.setPedido(rs.getFloat(1));
                bep.setLinea(rs.getFloat(2));
                bep.setFecha(rs.getString(3));
                bep.setFechaCli(rs.getString(4));
                bep.setExpeditacion(rs.getString(5));
                bep.setFechaPMP(rs.getString(6));
                bep.setStatus(rs.getString(7));
                bep.setCondicion(rs.getString(8));
                bep.setCliente(rs.getString(9));
                vec.addElement(bep);
            }   
            //System.out.println("SALIO DE listarPedidosEquiposEnProduccion()");
            conEns.close();
            st.close();
            rs.close();
            //System.out.println("VECTOR1:"+vec1);
        }catch(Exception e){
            e.printStackTrace();
        }
        //log.debug("Salio del Metodo consultaSQL");
        return vec;
    } 
    
        
      // Listar todos las Expeditaciones dado un  Pedidos y su linea
        public Vector listarExpeditacionEquiposEnProduccion(String Pedido,String Linea){
        int i=0;
        Vector vec= new Vector();
        try{
            //conectarShaffner();
             conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            //System.out.println("HOLAAAA");
            //System.out.println("ENTRO A listarExpeditacionEquiposEnProduccion()");
            String sql="";
            sql = sql + " select  convert(varchar,FechaDeAlta,111),Usuario,     \n";
            sql = sql + "         convert(varchar,FechaReprog,111),Comentario   \n";
            sql = sql + " from 	  ENSchaffner.dbo.TReprograma                   \n";
            sql = sql + " where   Pedido='"+Pedido+"'                           \n";
            sql = sql + " and     Linea='"+Linea+"'                             \n";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while(rs.next()){
                BeanExpeditacionDeEquipos bede = new BeanExpeditacionDeEquipos();
                bede.setFecha(rs.getString(1));
                bede.setUsuario(rs.getString(2));
                bede.setFechaReprogramacion(rs.getString(3));
                bede.setObservacion(rs.getString(4));
                vec.addElement(bede);
            }   
            //System.out.println("SALIO DE listarPedidosEquiposEnProduccion()");
            conEns.close();
            st.close();
            rs.close();
            //System.out.println("VECTOR1:"+vec1);
        }catch(Exception e){
            e.printStackTrace();
        }
        //log.debug("Salio del Metodo consultaSQL");
        return vec;
    } 
         // Listar todos las Expeditaciones dado un  Pedidos y su linea
    public Vector listarAvancesEquiposEnProduccion(String Pedido,String Linea){
        int i=0;
        Vector vec= new Vector();
        System.out.println("ENTRO AL NUEVO PAQUETE");
        try{
            //conectarShaffner();
            conEns = myConexion.getENSConnector();
            st = conEns.createStatement();
            //System.out.println("HOLAAAA");
            //System.out.println("ENTRO A listarExpeditacionEquiposEnProduccion()");
            String sql="";
            sql = sql + " select Semielaborado,convert(varchar,FechaInicio,111),    \n";
            sql = sql + "        convert(varchar,FechaTermino,111),Observacion      \n";
            sql = sql + " from 	ENSchaffner.dbo.TAvance                             \n";
            sql = sql + " where   Pedido='"+Pedido+"'                               \n";
            sql = sql + " and     Linea='"+Linea+"'                                 \n";
            sql = sql + " order 	by Semielaborado                            \n";
            
            System.out.println(sql);
            rs = st.executeQuery(sql);
            while(rs.next()){
                BeanAvanceDeEquipos bade = new BeanAvanceDeEquipos();
                bade.setSemielaborado(rs.getString(1));
                bade.setFechaInicio(rs.getString(2));
                bade.setFechaTermino(rs.getString(3));
                bade.setObservacion(rs.getString(4));
                vec.addElement(bade);
            }   
            //System.out.println("SALIO DE listarPedidosEquiposEnProduccion()");
            conEns.close();
            st.close();
            rs.close();
            //System.out.println("VECTOR1:"+vec1);
        }catch(Exception e){
            e.printStackTrace();
        }
        //log.debug("Salio del Metodo consultaSQL");
        return vec;
    } 
        
        
}

