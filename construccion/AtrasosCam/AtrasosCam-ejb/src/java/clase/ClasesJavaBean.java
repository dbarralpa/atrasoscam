/*
 * ClasesJavaBean.java
 *
 * Created on 31 de agosto de 2007, 11:18 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package clase;

import java.util.Date;

/** 
 *
 * @author mrojas
 */ 
public class ClasesJavaBean {
    private Date FechaCli;
    private Date FechaBodega;
    private String RutCliente; 
    private String Descri;
    private Date Expeditacion;
    private String Cliente;
    private String Serie;
    private int Pedido;
    private int Linea;
    private String Familia ;
    private String KVA;
    private String Proyecto;
    private int Multa;
    private String OrdenDeCompra;
    private float PorcMulta;
    private float ValorMulta;
    private float DiasAtraso;
    private String CartaAviso;
    private Date FechaAviso;
    private float Monto;
    private int Atraso;
    
    /** Creates a new instance of ClasesJavaBean */
    public ClasesJavaBean() {
    }

    public Date getFechaCli() {
        return FechaCli;
    }

    public void setFechaCli(Date FechaCli) {
        this.FechaCli = FechaCli;
    }

    public Date getFechaBodega() {
        return FechaBodega;
    }

    public void setFechaBodega(Date FechaBodega) {
        this.FechaBodega = FechaBodega;
    }

    public String getRutCliente() {
        return RutCliente;
    }

    public void setRutCliente(String RutCliente) {
        this.RutCliente = RutCliente;
    }

    public String getDescri() {
        return Descri;
    }

    public void setDescri(String Descri) {
        this.Descri = Descri;
    }

    public Date getExpeditacion() {
        return Expeditacion;
    }

    public void setExpeditacion(Date Expeditacion) {
        this.Expeditacion = Expeditacion;
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }

    public String getSerie() {
        return Serie;
    }

    public void setSerie(String Serie) {
        this.Serie = Serie;
    }

    public int getPedido() {
        return Pedido;
    }

    public void setPedido(int Pedido) {
        this.Pedido = Pedido;
    }

    public int getLinea() {
        return Linea;
    }

    public void setLinea(int Linea) {
        this.Linea = Linea;
    }

    public String getFamilia() {
        return Familia;
    }

    public void setFamilia(String Familia) {
        this.Familia = Familia;
    }

    public String getKVA() {
        return KVA;
    }

    public void setKVA(String KVA) {
        this.KVA = KVA;
    }

    public String getProyecto() {
        return Proyecto;
    }

    public void setProyecto(String Proyecto) {
        this.Proyecto = Proyecto;
    }

    public int getMulta() {
        return Multa;
    }

    public void setMulta(int Multa) {
        this.Multa = Multa;
    }

    public String getOrdenDeCompra() {
        return OrdenDeCompra;
    }

    public void setOrdenDeCompra(String OrdenDeCompra) {
        this.OrdenDeCompra = OrdenDeCompra;
    }

    public float getPorcMulta() {
        return PorcMulta;
    }

    public void setPorcMulta(float PorcMulta) {
        this.PorcMulta = PorcMulta;
    }

    public float getValorMulta() {
        return ValorMulta;
    }

    public void setValorMulta(float ValorMulta) {
        this.ValorMulta = ValorMulta;
    }

    public float getDiasAtraso() {
        return DiasAtraso;
    }

    public void setDiasAtraso(float DiasAtraso) {
        this.DiasAtraso = DiasAtraso;
    }

    public String getCartaAviso() {
        return CartaAviso;
    }

    public void setCartaAviso(String CartaAviso) {
        this.CartaAviso = CartaAviso;
    }

    public Date getFechaAviso() {
        return FechaAviso;
    }

    public void setFechaAviso(Date FechaAviso) {
        this.FechaAviso = FechaAviso;
    }

    public float getMonto() {
        return Monto;
    }

    public void setMonto(float Monto) {
        this.Monto = Monto;
    }

    public int getAtraso() {
        return Atraso;
    }

    public void setAtraso(int Atraso) {
        this.Atraso = Atraso;
    }
    
    
}
