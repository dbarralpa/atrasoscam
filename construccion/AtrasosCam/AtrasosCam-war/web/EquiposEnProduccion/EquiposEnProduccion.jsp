<%@ page import="java.io.*,java.util.*,javax.naming.*,mac.contador.Contador,javax.rmi.PortableRemoteObject,mac.equipoenproduccion.*,mac.BeansEquiposEnProduccion.*,java.rmi.RemoteException,java.util.Vector,java.sql.*,java.text.*" session="true" %>
<!--jsp:useBean id="epBean" scope="page" class="mac.equipoenproduccion.EquipoEnProduccionBean" /-->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
 <%
 String usuario = request.getParameter("usuario");
 String tipou = request.getParameter("tipou");
 HttpSession sesion = request.getSession();
 String idSesion =  request.getParameter("idSesion");

 NumberFormat nf1 = NumberFormat.getInstance();
 nf1.setMaximumFractionDigits(1);
 nf1.setMinimumFractionDigits(0);
 %>
<html>
    <head>
        <title>Equipos en Producci&oacute;n</title>
    </head>
    <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    <body>    
        <%
            String pedido=request.getParameter("pedido");
            if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
              Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
        }
            if(idSesion.equals(sesion.getId())){
        EquipoEnProduccion1Remote man =null;
        try{
            conexionEJB.ConexionEJB cal = new conexionEJB.ConexionEJB();
            
            Object ref1 =cal.conexionBean("EquipoEnProduccion1Bean");
            EquipoEnProduccion1RemoteHome manRemoteHome = (EquipoEnProduccion1RemoteHome) PortableRemoteObject.narrow(ref1, EquipoEnProduccion1RemoteHome.class);
            man = manRemoteHome.create();
        }catch(Exception e){
            out.println("ERROR1: pagina buscarClientesPorLetra: " + e.getMessage() );
        }
            
        %>
        <table width="70%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><h1><font class="Estilo10 Estilo12">Equipos en Producci&oacute;n</font></h1></td>
            </tr>
            <tr>
                <td>
                    <form name="form1" action="EquiposEnProduccion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                    <font class="Estilo10">Pedido&nbsp;:&nbsp;</font>
                                    <input type="text" name="pedido" maxlength="10" size="15" class="Estilo10" <%if(pedido==null){%>value=""<%}else{%>value="<%=pedido%>"<%}%>>
                                    <font class="Estilo10">&nbsp;&nbsp;</font>
                                    <input type="submit" name="si" value="Siguiente" class="Estilo9">
                                </td>       
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
        <%
            Vector vectorEEP = new Vector();
            vectorEEP=man.listarPedidosEquiposEnProduccion(pedido);
            Enumeration eEEP = vectorEEP.elements();
            if(vectorEEP.size()>0){
        %>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td width="40%">
                    <table border="1" cellpadding="0" cellspacing="0" width="300" align="center" bgcolor="#CECECE">
                        <tr bgcolor="#B9DDFB">
                            <!--<td align="center"><font class="Estilo10">Pedido</font></td>-->
                            <td align="center" width="40"><font class="Estilo10">L&iacute;nea</font></td>
                            <!--<td align="center"><font class="Estilo10">Pedido</font></td>-->
                            <td align="center" width="80"><font class="Estilo10">F.Cliente</font></td>
                            <td align="center" width="80"><font class="Estilo10">F.Exped.</font></td>
                            <!--<td align="center"><font class="Estilo10">PMP</font></td>-->
                            <td align="center" width="100"><font class="Estilo10">Estatus</font></td>
                            <!--<td align="center"><font class="Estilo10">Condicion</font></td>
                            <td align="center"><font class="Estilo10">Razon Social</font></td>-->
                        </tr>
                        <% while(eEEP.hasMoreElements()){
                            BeansEquiposEnProduccion ep=(BeansEquiposEnProduccion)eEEP.nextElement();
                             String linea1=toString().valueOf(ep.getLinea());%>
                        <tr>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=nf1.format(ep.getPedido())%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=nf1.format(ep.getLinea())%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getFecha()%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getFechaCli()%></a></font></td>
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getExpeditacion()%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getFechaPMP()%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getStatus()%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//if(ep.getCondicion()==null){%>&nbsp;<%//}else{%><%//=ep.getCondicion()%><%//}%></a></font></td>
                            <td align="right"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getCliente()%></a></font></td>-->
                        </tr>
                        <%}%>
                    </table>
                    <%}else{
                        if(pedido!=null){%>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                        <tr>
                            <td><br><br><br><br></td>
                        </tr>
                        <tr>
                            <td align="center"><font class="Estilo10">No existe en nuestros registros el Pedido: <%=pedido%> solicitado</font></td>
                        </tr>
                    </table>
                    <%  }
                    }%>
                </td>
                <td width="60%">
                    &nbsp;
                </td>
            </tr>
        </table>    
        
        <table cellpadding="0" cellspacing="0" border="0" width="90%">
            <td align="right">
                <br>
                <a href="/mac/jsp/dependencias/Comercial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../images/volver2.jpg" border="0" width="89" height="18" alt="Regresar a la p&aacute;gina anteriror"/></a>
            </td>
        </table>
        <%}else{response.sendRedirect("../mac.sc.sis.inicio-war");}%> 
    </body>
</html>
