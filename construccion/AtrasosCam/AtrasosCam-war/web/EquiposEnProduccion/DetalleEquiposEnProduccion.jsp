<%@ page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.equipoenproduccion.*,mac.BeansEquiposEnProduccion.*,java.rmi.RemoteException,java.util.Vector,java.sql.*,java.text.*" session="true" %>
<!--jsp:useBean id="epBean" scope="page" class="mac.equipoenproduccion.EquipoEnProduccionBean"/-->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%
String usuario = request.getParameter("usuario");
String tipou = request.getParameter("tipou");
HttpSession sesion = request.getSession();
String idSesion =  request.getParameter("idSesion");
NumberFormat nf1 = NumberFormat.getInstance();
nf1.setMaximumFractionDigits(1);
nf1.setMinimumFractionDigits(0);
%>

<html>
    <head>
        <title>Equipos en Producci&oacute;n</title>
    </head>
    <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    <body>    
        <%
            String pedido=request.getParameter("pedido");
            String linea=request.getParameter("linea");
            String fechaPMP=request.getParameter("fechaPMP");
            String cliente=request.getParameter("cliente");
            
            if(idSesion.equals(sesion.getId())){
        EquipoEnProduccion1Remote man =null;
        try{
            conexionEJB.ConexionEJB cal = new conexionEJB.ConexionEJB();
            
            Object ref1 =cal.conexionBean("EquipoEnProduccion1Bean");
            EquipoEnProduccion1RemoteHome manRemoteHome = (EquipoEnProduccion1RemoteHome) PortableRemoteObject.narrow(ref1, EquipoEnProduccion1RemoteHome.class);
            man = manRemoteHome.create();
        }catch(Exception e){
            out.println("ERROR1: pagina buscarClientesPorLetra: " + e.getMessage() );
        }
        %>
        <table width="70%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><h1><font class="Estilo10 Estilo12">Equipos en Producci&oacute;n</font></h1></td>
            </tr>
            <tr>
                <td>
                    <form name="form1" action="EquiposEnProduccion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <font class="Estilo10">Pedido&nbsp;:&nbsp;</font>
                                <input type="text" name="pedido" maxlength="10" size="15" class="Estilo10" <%if(pedido==null){%>value=""<%}else{%>value="<%=pedido%>"<%}%>>
                                <font class="Estilo10">&nbsp;&nbsp;</font>
                                <input type="submit" name="si" value="Siguiente" class="Estilo9">
                            </td>       
                        </tr>
                    </table>
                    </form>
                </td>
            </tr>
        </table>
        
        <%Vector vectorEEP = new Vector();
          vectorEEP=man.listarPedidosEquiposEnProduccion(pedido);
          Enumeration eEEP = vectorEEP.elements();
          if(vectorEEP.size()>0){%>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td width="40%" valign="top">
                    <table border="1" cellpadding="0" cellspacing="0" width="300" align="center" bgcolor="#CECECE">
                        <!--<tr bgcolor="#B9DDFB">
                            <td colspan="2" width="10%" align="center"><font class="Estilo10">Pedido</font></td>
                            <td colspan="4" width="28%" align="center"><font class="Estilo10">Fecha</font></td>
                            <td colspan="2" width="16%" align="center"><font class="Estilo10">Estado</font></td>
                            <td width="28%" align="center"><font class="Estilo10">Cliente</font></td>
                        </tr>-->
                        <tr bgcolor="#B9DDFB">
                            <!--<td align="center"><font class="Estilo10">Pedido</font></td>-->
                            <td  align="center" width="40"><font class="Estilo10">L&iacute;nea</font></td>
                            <!--<td align="center"><font class="Estilo10">Pedido</font></td>-->
                            <td align="center" width="80"><font class="Estilo10">F.Cliente</font></td>
                            <td align="center" width="80"><font class="Estilo10">F.Exped.</font></td>
                            <!--<td align="center"><font class="Estilo10">PMP</font></td>-->
                            <td align="center" width="100"><font class="Estilo10">Estatus</font></td>
                            <!--<td align="center"><font class="Estilo10">Condicion</font></td>
                            <td align="center"><font class="Estilo10">Razon Social</font></td>-->
                        </tr>
                        <%while(eEEP.hasMoreElements()){
                          BeansEquiposEnProduccion ep=(BeansEquiposEnProduccion)eEEP.nextElement();
                          String linea1=toString().valueOf(ep.getLinea());%>
                        <tr>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=nf1.format(ep.getPedido())%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=nf1.format(ep.getLinea())%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getFecha()%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getFechaCli()%></a></font></td>
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getExpeditacion()%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getFechaPMP()%></a></font></td>-->
                            <td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%=linea1%>&pedido=<%=pedido%>&fechaPMP=<%=ep.getFechaPMP()%>&cliente=<%=ep.getCliente()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><%=ep.getStatus()%></a></font></td>
                            <!--<td align="center"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//if(ep.getCondicion()==null){%>&nbsp;<%//}else{%><%//=ep.getCondicion()%><%//}%></a></font></td>
                            <td align="right"><font class="Estilo10"><a href="DetalleEquiposEnProduccion.jsp?linea=<%//=linea1%>&pedido=<%//=pedido%>"><%//=ep.getCliente()%>&nbsp;</font></td>-->
                        </tr>              
                        <%}%>
                    </table>
                    <%}else{
                        if(pedido!=null){%>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                        <tr>
                            <td><br><br><br><br></td>
                        </tr>
                        <tr>
                            <td align="center"><font class="Estilo10">No existe en nuestros registros el Pedido: <%=pedido%> solicitado</font></td>
                        </tr>
                    </table>
                    <%}}%>
                </td>
                <td width="60%">
                    <% float linea2=Float.parseFloat(linea);%>
                    <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="90" bgcolor="#B9DDFB"><font class="Estilo10">Pedido &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font></td>
                            <td width="410"><font class="Estilo10">&nbsp;</font><font class="Estilo10" style='background-color:#CECECE;'><%=pedido%>-<%=nf1.format(linea2)%></font></td>
                        </tr>
                        <tr>
                            <td bgcolor="#B9DDFB"><font class="Estilo10">Fecha Pedido :</font></td>
                            <td><font class="Estilo10">&nbsp;</font><font class="Estilo10" style='background-color:#CECECE;'><%=fechaPMP%></font></td>
                        </tr>
                        <tr>
                            <td bgcolor="#B9DDFB"><font class="Estilo10">Raz&oacute;n Social&nbsp; :</font></td>
                            <td><font class="Estilo10">&nbsp;</font><font class="Estilo10" style='background-color:#CECECE;'><%=cliente%></font></td>
                        </tr>
                        <tr>
                            <td><font class="Estilo10">&nbsp;</font></td>
                            <td><font class="Estilo10">&nbsp;</font></td>
                        </tr>
                    </table>
                    
                    <table><tr><td>&nbsp;</td></tr>
                    </table>
                    <% //EXPEDITACION
                        Vector vectorExp = new Vector();
                        vectorExp=man.listarExpeditacionEquiposEnProduccion(pedido,linea);
                        Enumeration eExp = vectorExp.elements();
                        if(vectorExp.size()>0){
                    %>
                    <table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
                        <tr>
                            <td align="left"><font class="Estilo10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expeditaci&oacute;n</font></td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="1" width="500" align="center" bgcolor="#CECECE">
                        <tr bgcolor="#B9DDFB">
                            <td align="center" width="80"><font class="Estilo10">Fecha</font></td>
                            <td align="center" width="80"><font class="Estilo10">Usuario</font></td>
                            <td align="center" width="80"><font class="Estilo10">F.Reprog</font></td>
                            <td align="center" width="260"><font class="Estilo10">Observaci&oacute;n</font></td>
                        </tr>
                        <%while(eExp.hasMoreElements()){
                            BeanExpeditacionDeEquipos bede=(BeanExpeditacionDeEquipos)eExp.nextElement();%>
                        <tr>
                            <td align="center"><font class="Estilo10"><%=bede.getFecha()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bede.getUsuario()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bede.getFechaReprogramacion()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bede.getObservacion()%>&nbsp;</font></td>
                        </tr>
                        <%}%>                        
                    </table>
                    <table><tr><td>&nbsp;</td></tr>
                    </table>
                    <%}//END  if(vectorExp.size()>0)%>

                    <%  //AVANCE
                        Vector vectorAva = new Vector();
                        vectorAva=man.listarAvancesEquiposEnProduccion(pedido,linea);
                        Enumeration eAva = vectorAva.elements();
                        if(vectorAva.size()>0){
                    %>
                    <table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
                        <tr>
                            <td align="left"><font class="Estilo10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Avance</font></td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" border="1" width="500" align="center" bgcolor="#CECECE">
                        <tr bgcolor="#B9DDFB">
                            <td align="center" width="80"><font class="Estilo10">Proceso</font></td>
                            <td align="center" width="80"><font class="Estilo10">Inicio</font></td>
                            <td align="center" width="80"><font class="Estilo10">T&eacute;rmino</font></td>
                            <td align="center" width="260"><font class="Estilo10">Observaci&oacute;n</font></td>
                        </tr>
                        <%while(eAva.hasMoreElements()){
                            BeanAvanceDeEquipos bade=(BeanAvanceDeEquipos)eAva.nextElement();%>
                        <tr>
                            <td align="center"><font class="Estilo10"><%=bade.getSemielaborado()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bade.getFechaInicio()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bade.getFechaTermino()%>&nbsp;</font></td>
                            <td align="center"><font class="Estilo10"><%=bade.getObservacion()%>&nbsp;</font></td>
                        </tr>
                        <%}%>
                    </table>
                    <%}//END  if(vectorExp.size()>0)%>

                </td>
            </tr>
        </table>
        
        <table cellpadding="0" cellspacing="0" border="0" width="90%">
            <td align="right">
                <br>
                <a href="mac/jsp/dependencias/Comercial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../images/volver2.jpg" border="0" width="89" height="18" alt="Regresar a la p&aacute;gina anteriror"/></a>
            </td>
        </table>
        <%}else{response.sendRedirect("../mac.sc.sis.inicio-war");}%> 
    </body>
</html>
